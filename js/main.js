jQuery(document).ready(function($) {
	var $modal = $('.custom-box-search-head');

	$('.js-open-modal').on('click', openModal);

	$modal
		.on('click', closeModal)
		.on('click', '.search-head', function(event) {
		event.stopPropagation();  // prevent closing the modal window when user clicks on the window itself s
	});

	function closeModal() {
		$modal.fadeOut();
	};

	function openModal() {
		$modal.fadeIn();
	};
});



$(window).scroll(function () {
  let contentHeadHeigh = $("header").height();
  var sticky = $('header'),
    scroll = $(window).scrollTop();

  if (scroll >= contentHeadHeigh) sticky.addClass('menu-scroll');
  if (scroll == 0)
    sticky.removeClass('menu-scroll');
});
// Load srcoll bar process and load header footer page
$(document).ready(function () {
  $(".header-load").load("header.html");
  $(".footer-load").load("footer.html");
});
window.onscroll = function () { myFunction() };
function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("myBar").style.width = scrolled + "%";
}

// Js click menu
$(".ic-menu-mb").click(function () {
  $('header').addClass('show-menu');
});

$(".ic-close-menu-mb").click(function () {
  $('header').removeClass('show-menu');
});
// Js click search mobile

$(".ic-search-mb").click(function () {
  $('.search-head').addClass('show-search-mb');
});
$(".btn-close-search-mb").click(function () {
  $('.search-head').removeClass('show-search-mb');
});

// Js srcoll to fixed box title page course detail
$(window).scroll(function () {
  let contentHeadHeigh = $("body").height();
  var sticky = $('.fixed-title'),
    scroll = $(window).scrollTop();

  if (scroll >= 200) sticky.addClass('show-fixed-title');
  if (scroll == 0)
    sticky.removeClass('show-fixed-title');
});

$('.btn-action a').click(function () {
  var href = $(this).attr('href');
  var anchor = $(href).offset();
  window.scrollTo(anchor.left, anchor.top - 150);
  return false;
});

// Js Click show more page list course
$(".view-more-course").click(function () {
  $('.content-box-course').addClass('show-more-list-course');
});

// Js expand nut " xem them "
$(".expand-content-editor").click(function () {
  $(".description-body").toggleClass("heightAuto");
});


function inVisible(element) {
  //Checking if the element is
  //visible in the viewport
  var WindowTop = $(window).scrollTop();
  var WindowBottom = WindowTop + $(window).height();
  var ElementTop = element.offset().top;
  var ElementBottom = ElementTop + element.height();
  //animating the element if it is
  //visible in the viewport
  if ((ElementBottom <= WindowBottom) && ElementTop >= WindowTop)
    animate(element);
}


/* JS Select box*/
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function (e) {
      /*when an item is clicked, update the original select box,
      and the selected item:*/
      var y, i, k, s, h, sl, yl;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      sl = s.length;
      h = this.parentNode.previousSibling;
      for (i = 0; i < sl; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName("same-as-selected");
          yl = y.length;
          for (k = 0; k < yl; k++) {
            y[k].removeAttribute("class");
          }
          this.setAttribute("class", "same-as-selected");
          break;
        }
      }
      h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function (e) {
    /*when the select box is clicked, close any other select boxes,
    and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);


// Elements Slider Page Libary //
$(document).ready(function () {
  $('.slider-libary').owlCarousel({
    loop: true,
    margin: 10,
    // autoplayHoverPause: false,
    // autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

// Loading page
$(window).on('DOMContentLoaded', function () {

  if ($('.spinner-container').length) {
    $('.spinner-container').delay(600).fadeOut('500');
  }
});

